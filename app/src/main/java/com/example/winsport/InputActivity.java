
package com.example.winsport;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.winsport.data.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class InputActivity extends AppCompatActivity {

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        fulscreen();
        return super.dispatchTouchEvent( event );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.personal_data_input);
        fulscreen();
        EditText nameInput = findViewById(R.id.name_input);
        EditText heightInput = findViewById(R.id.height_input);
        EditText weightInput = findViewById(R.id.weight_input);

        Button button = findViewById(R.id.input_data_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteData();
                if (isNumeric(heightInput.getText().toString()) && isNumeric(weightInput.getText().toString())){
                    writeFile(nameInput.getText().toString(),heightInput.getText().toString(),weightInput.getText().toString());
                }
                else Toast.makeText(getApplicationContext(),"Вес и рост должны быть числами",Toast.LENGTH_LONG).show();

                if(getString() != null) {
                    finish();
                }
            }
        });

    }

    public void writeFile(String name, String height, String weight){ //name 1 ; height 2; weight 3;

        try {
            File data = new File(getApplicationContext().getFilesDir(), Constants.personalDataFile);
            if (!data.exists()) {
                data.createNewFile();
            }
            FileWriter writer = new FileWriter(data);
            writer.append(name).append("\n");
            writer.append(height).append("\n");
            writer.append(weight);
            writer.flush();
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    private String getString() {
        String string = "";
        File data = new File(getApplicationContext().getFilesDir(), Constants.personalDataFile);
        if (data != null) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(data));
                String line;

                while ((line = reader.readLine()) != null) {
                    string += line;
                    string += "\n";
                }

                reader.close();
                return string;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void deleteData() {
        File data = new File(getApplicationContext().getFilesDir(), Constants.personalDataFile);
        data.delete();
        File progress = new File(getApplicationContext().getFilesDir(), Constants.progressFile);
        progress.delete();
    }
    private void fulscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private  boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}