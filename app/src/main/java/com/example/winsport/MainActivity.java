package com.example.winsport;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.winsport.data.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.intro_layout);
        fulscreen();

        Handler handler = new Handler();
        Intent intent = new Intent(this, InputActivity.class);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getData(3) == null) {
                    startActivity(intent);
                    setContentView(R.layout.activity_main);
                }
                else{
                    setContentView(R.layout.activity_main);
                }
            }
        }, 3000);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        fulscreen();
        return super.dispatchTouchEvent( event );
    }

    public String getData(int i) {
        String string = null;
        int j = 0;
        File data = new File(this.getFilesDir(), Constants.personalDataFile);
        if (data.exists()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(data));
                String line;

                while ((line = reader.readLine()) != null) {
                    j++;
                    if(j == i) string = line;
                }
                reader.close();
                return string;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void fulscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}