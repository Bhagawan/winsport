package com.example.winsport.data;

public class Constants {
    public static final int NAME = 1;
    public static final int HEIGHT = 2;
    public static final int WEIGHT = 3;
    public static final int LAST_PROGRESS_DATA = 1;
    public static final int LAST_PROGRESS_POINTS = 2;
    public static final String personalDataFile= "personal_data.txt";
    public static final String progressFile= "progress.txt";
}
