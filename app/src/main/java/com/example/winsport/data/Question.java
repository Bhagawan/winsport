package com.example.winsport.data;

public class Question {
    public String question, answer;
    public int id;
    public Question(String question, String answer, int id) {
        this.id = id;
        this.question = question;
        this.answer = answer;
    }
}
