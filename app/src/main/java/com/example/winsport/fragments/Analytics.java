package com.example.winsport.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.winsport.R;
import com.example.winsport.data.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Analytics extends Fragment {
    private View view;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(requireActivity(),R.id.fragmentContainerView).navigate(R.id.mainMenu);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.analytics_layout, parent, false);
        updateProgress();

        EditText distanceInput = view.findViewById(R.id.distance_input);
        EditText squatInput = view.findViewById(R.id.squat_input);

        Button input = view.findViewById(R.id.analytics_input_button);
        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNumeric(distanceInput.getText().toString()) && isNumeric(squatInput.getText().toString())) {
                    int progress = Integer.parseInt(distanceInput.getText().toString()) + (Integer.parseInt(squatInput.getText().toString()) * 10);
                    String weight = getData(Constants.WEIGHT,Constants.personalDataFile);
                    if (weight != null) {
                        progress = (progress * Integer.parseInt(weight)) / 10;
                        addProgress(progress);
                        updateProgress();
                    }
                }
                else Toast.makeText(getContext(),"Очистите поле перед вводом",Toast.LENGTH_SHORT).show();

            }
        });

        ImageButton backbutton = view.findViewById(R.id.analytics_back_button);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.mainMenu);
            }
        });

        return view;
    }


    public void addProgress(int number){
        try {
            int lastProgress = 0;
            String lastData = getData( Constants.LAST_PROGRESS_DATA, Constants.progressFile);
            String readProgress = getData( Constants.LAST_PROGRESS_POINTS, Constants.progressFile);
            if (readProgress != null)
                lastProgress = Integer.parseInt(readProgress);

            File data = new File(getContext().getFilesDir(), Constants.progressFile);
            if (data.exists()) {
                data.delete();
            }
            data.createNewFile();

            FileWriter writer = new FileWriter(data);

            SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yy", Locale.getDefault());

             if (sd.getDateInstance().toString().equals(lastData)){
                 lastProgress += number;
             }
             else lastProgress = number;

            writer.append(sd.getDateInstance().toString()).append("\n");
            writer.append(Integer.toString(lastProgress));
            writer.flush();
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private String getData(int i, String file) {
        String string = null;
        int j = 0;
        File data = new File(getContext().getFilesDir(), file);
        if (data != null) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(data));
                String line;

                while ((line = reader.readLine()) != null) {
                    j++;
                    if(j == i) string = line;
                }
                reader.close();
                return string;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void updateProgress() {
        TextView amount = view.findViewById(R.id.analytics_progress_amount);
        String points = getData( Constants.LAST_PROGRESS_POINTS,Constants.progressFile);
        if(points == null) points = "0";
        String output = "<font color='#FF5722'>"+ points + "</font>" + "P";
        amount.setText(Html.fromHtml(output));
    }

    private  boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
