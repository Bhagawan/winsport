package com.example.winsport.fragments;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.winsport.R;
import com.example.winsport.data.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class MainMenu extends Fragment {
    private View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.menu_layout, parent, false);
        updateProgress();

        Button settingsButton = view.findViewById(R.id.main_menu_settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.settings_fragment);
            }
        });
        Button analyticsButton = view.findViewById(R.id.main_menu_analytics_button);
        analyticsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.analytics_fragment);
            }
        });
        Button trainingButton = view.findViewById(R.id.main_menu_training_button);
        trainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.training_fragment);
            }
        });
        Button questionButton = view.findViewById(R.id.main_menu_trener_button);
        questionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.questions_fragment);
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateProgress();
    }

    private void updateProgress() {
        TextView amount = view.findViewById(R.id.main_menu_progress_output);
        String points = getData( Constants.LAST_PROGRESS_POINTS, Constants.progressFile);
        if(points == null) points = "0";
        String output = "<font color='#FF5722'>"+ points + "</font>" + "P";
        amount.setText(Html.fromHtml(output));
        TextView goal = view.findViewById(R.id.textView);
        String goaloutput = "Цель на день: <font color='#FF5722'>25000</font>Points";
        goal.setText(Html.fromHtml(goaloutput));
    }

    private String getData(int i, String file) {
        String string = null;
        int j = 0;
        File data = new File(getContext().getFilesDir(), file);
        if (data != null) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(data));
                String line;

                while ((line = reader.readLine()) != null) {
                    j++;
                    if(j == i) string = line;
                }
                reader.close();
                return string;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
