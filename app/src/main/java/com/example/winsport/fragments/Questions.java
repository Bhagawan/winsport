package com.example.winsport.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.winsport.R;
import com.example.winsport.data.RequestHistory;
import com.example.winsport.utility.RequestResult;
import com.example.winsport.utility.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;


public class Questions extends Fragment{
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.question_layout, parent, false);

        EditText input = view.findViewById(R.id.question_input);
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });


        ImageButton backButton = view.findViewById(R.id.training_back_button2);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.mainMenu);
            }
        });

        ImageButton inputButton = view.findViewById(R.id.question_input_button);
        inputButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ask(input.getText().toString());
                InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                setWaiting();
            }
        });

        return view;
    }

    private void ask(String question) {
        ServerRequest request = new ServerRequest();
        int id =(int) SystemClock.uptimeMillis();
        request.result = new RequestResult() {
            @Override
            public void handleResult(JSONObject output) throws JSONException {
                String response = output.getString("response");
                RequestHistory.addQuestion(question, response, id);
                updateHistory();
            }
        };
        request.execute(question,Integer.toString(id));
    }

    private void updateHistory() {
        LinearLayout ll = view.findViewById(R.id.history_output);
        ll.removeAllViews();
        if (RequestHistory.history.size() > 0)
            for(int i = RequestHistory.history.size(); i > 0; i--) {
                TextView question = new TextView(getContext());
                question.setText(RequestHistory.history.get(i-1).question);
                question.setTextColor(Color.parseColor("#FF5722"));
                question.setTextSize(20);
                ll.addView(question, new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));

                TextView answer = new TextView(getContext());
                answer.setText(RequestHistory.history.get(i-1).answer);
                answer.setTextSize(20);
                answer.setTextColor(Color.parseColor("#FFFFFF"));
                ll.addView(answer, new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
            }
    }

    private void setWaiting() {
        LinearLayout ll = view.findViewById(R.id.history_output);
        ll.removeAllViews();
        TextView waiting = new TextView(getContext());
        waiting.setText("Ответ обрабатывается");
        waiting.setTextSize(20);
        waiting.setTextColor(Color.parseColor("#FFFFFF"));
        ll.addView(waiting, new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));

    }

}
