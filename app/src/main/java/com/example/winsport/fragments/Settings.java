package com.example.winsport.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.winsport.InputActivity;
import com.example.winsport.R;
import com.example.winsport.data.Constants;

import java.io.File;

public class Settings extends Fragment {
    private View view;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(requireActivity(),R.id.fragmentContainerView).navigate(R.id.mainMenu);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.settings_layout, parent, false);
        Button clear = view.findViewById(R.id.clear_data_button);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteData();
                Intent intent = new Intent(getContext(), InputActivity.class);
                startActivity(intent);
                Navigation.findNavController(v).navigate(R.id.mainMenu);
            }
        });

        ImageButton backbutton = view.findViewById(R.id.settings_back_button);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.mainMenu);
            }
        });


        return view;
    }

    private void deleteData() {
        File data = new File(getContext().getFilesDir(), Constants.personalDataFile);
        data.delete();
        File progress = new File(getContext().getFilesDir(), Constants.progressFile);
        progress.delete();
    }
}
