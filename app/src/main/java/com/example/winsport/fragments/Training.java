package com.example.winsport.fragments;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.winsport.R;
import com.example.winsport.utility.GetBitmap;
import com.example.winsport.utility.MyJsonUtil;
import com.example.winsport.utility.PictureDownloader;
import com.example.winsport.utility.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Training extends Fragment implements TaskResult {
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.training_layout, parent, false);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        TextView headliner = view.findViewById(R.id.training_headliner);
        MyJsonUtil task = new MyJsonUtil();
        task.result = this;
        task.execute("http://84.38.181.162/ios/"+dayOfTheWeek.toLowerCase(Locale.ENGLISH)+".json");
        switch(dayOfTheWeek) {
            case "Monday":
                headliner.setText(getResources().getString(R.string.monday));
                break;
            case "Tuesday":
                headliner.setText(getResources().getString(R.string.tuesday));
                break;
            case "Wednesday":
                headliner.setText(getResources().getString(R.string.wednesday));
                break;
            case "Thursday":
                headliner.setText(getResources().getString(R.string.thursday));
                break;
            case "Friday":
                headliner.setText(getResources().getString(R.string.friday));
                break;
            case "Saturday":
                headliner.setText(getResources().getString(R.string.saturday));
                break;
            case "Sunday":
                headliner.setText(getResources().getString(R.string.sunday));
                break;
        }

        ImageButton backbutton = view.findViewById(R.id.training_back_button);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.mainMenu);
            }
        });

        return view;
    }

    @Override
    public void handleResult(JSONArray output) throws JSONException {
        if(output != null) {
            JSONObject obj = (JSONObject) output.get(0);
            String text = obj.getString("text");
            String img = obj.getString("img");
            PictureDownloader pict = new PictureDownloader();
            pict.result = new GetBitmap() {
                @Override
                public void handleBitmap(Bitmap picture) {
                    addTraining(picture, text);

                }
            };
            pict.execute(img);
        }
        else Toast.makeText(getContext(),"Error.",Toast.LENGTH_SHORT).show();

    }

    private void  addTraining(Bitmap picture, String text) {
        LinearLayout ll = view.findViewById(R.id.training_output);

        if (picture != null ) {
            ImageView image = new ImageView(getContext());
            image.setImageBitmap(picture);
            image.setPadding(20,20,20,0);
            ll.addView(image, new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        }
        else Toast.makeText(getContext(),"Error loading image.",Toast.LENGTH_SHORT).show();

        TextView textView = new TextView(getContext());
        textView.setText(text);
        textView.setTop(10);
        textView.setTextSize(20);
        textView.setMaxLines(5);
        textView.setPadding(20,20,20,20);
        textView.setTextColor(getResources().getColor(R.color.white));
        ll.addView(textView, new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));

        Button button = new Button(getContext());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAnimator animation = ObjectAnimator.ofInt(textView, "maxLines", textView.getLineCount());
                animation.setDuration(200).start();
                button.setVisibility(View.GONE);
            }
        });
        button.setPadding(20,20,20,20);
        button.setBackgroundResource(R.drawable.ic_expand);
        button.setHeight(10);
        ll.addView(button, new LinearLayout.LayoutParams (LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT,2));

    }
}
