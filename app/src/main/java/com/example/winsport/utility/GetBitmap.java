package com.example.winsport.utility;

import android.graphics.Bitmap;

public interface GetBitmap {
    void handleBitmap(Bitmap picture);
}
