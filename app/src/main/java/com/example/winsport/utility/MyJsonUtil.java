package com.example.winsport.utility;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class MyJsonUtil extends AsyncTask<String, Void, JSONArray>{
    public TaskResult result = null;
    @Override
    protected JSONArray doInBackground(String... strings) {
        try {
            java.net.URL url = new java.net.URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream input = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            StringBuilder string = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                string.append(line).append('\n');
            }
            return new JSONArray(string.toString());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(JSONArray array) {
        try {
            result.handleResult(array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
