package com.example.winsport.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

public class PictureDownloader extends AsyncTask<String, Void, Bitmap> {
    public GetBitmap result = null;
    @Override
    protected Bitmap doInBackground(String... strings) {
        java.net.URL url = null;
        try {
            url = new java.net.URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
            //return getResizedBitmap(myBitmap,500,500);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
    protected void onPostExecute(Bitmap picture) {
        result.handleBitmap(picture);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
    }
}
