package com.example.winsport.utility;

import org.json.JSONException;
import org.json.JSONObject;

public interface RequestResult {
    void handleResult(JSONObject output) throws JSONException;
}
