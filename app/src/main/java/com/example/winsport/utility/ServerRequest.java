package com.example.winsport.utility;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class ServerRequest extends AsyncTask<String , Void, JSONObject> {
    public RequestResult result = null;

    @Override
    protected JSONObject doInBackground(String... strings) {

        HttpURLConnection connection = null;
        try {
            URL url = new URL("http://84.38.181.162/ios/ask.php");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/json; utf-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            String send = "{\"ask\": \"" + strings[0] + "\", \"id\": \"" + strings[1] + "\"}";
            try(OutputStream os = connection.getOutputStream()) {
                byte[] input = send.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            url = new URL("http://84.38.181.162/ios/response.php");
            HttpURLConnection resp = (HttpURLConnection) url.openConnection();
            resp.setRequestMethod("GET");

            try(BufferedReader br = new BufferedReader(new InputStreamReader(resp.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                return new JSONObject(response.toString());
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(JSONObject response) {
        if(response != null) {
            try {
                result.handleResult(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
