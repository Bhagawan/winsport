package com.example.winsport.utility;

import org.json.JSONArray;
import org.json.JSONException;

public interface TaskResult {
    void handleResult(JSONArray output) throws JSONException;
}
